$(document).ready(function () {
    //Add local scroll
    $.localScroll();
    //Set font size based on current width
    function defineWindowWidth() {
        if ($(window).width() > 1300) {
            $('html').css('font-size', $(window).width() / 100);
        }
    }

    function defineScmImg(){
        //determine scheme img dimensions
        if ($('#shemeImgMain').height()){
            $('.imgMarks').height($('#shemeImgMain').height());
        }else{
            $('.imgMarks').height('100%');
        }
        if ($('#schemeImg #shemeImgMain').width()){
            $('.imgMarks').width($('#shemeImgMain').width());
        }else{
            $('.imgMarks').width('100%');
        }
    }

//Change font size, if screen width changed
    $(window).resize(function () {
        defineWindowWidth();
        $("#accordion").accordion("refresh");
        if ($(window).width() < 1200) {
            $('html').css('font-size', '14');
        }
        defineScmImg();
    });
//Init main slider
    var owlMain = $("#mainSlider");
    owlMain.owlCarousel({
        navigation: true, // Show next and prev buttons
        slideSpeed: 300,
        paginationSpeed: 400,
        navigationText: 0,
        singleItem: true
    });
    //Init mobile app slider
    var time = 3; // time in seconds
    var $progressBar,
        $bar,
        $elem,
        isPause,
        tick,
        percentTime;
    var owlApp = $("#mobileSlider");
    owlApp.owlCarousel({
        navigation: false,
        slideSpeed: 500,
        paginationSpeed: 500,
        pagination: false,
        singleItem: true,
        //autoPlay: 7000,
        afterMove: moved,
        afterInit: progressBar,
        addClassActive: true
    });
    //Get data from Owl
    var owlData = owlApp.data('owlCarousel');
    function afterAction(){
        var owlCurrentItem = this.owl.currentItem;
        //makeOpacity(owlCurrentItem);
    }
    // Custom Navigation Events
    $("#appNav li").click(function () {
        owlData.goTo($(this).index());
    });
    //Init progressBar
    function progressBar($elem) {
        $elem = $("#mobileSlider");
        //build progress bar elements
        console.log(this.owl.currentItem);
        buildProgressBar();
        //start counting
        start();
    }
    //create div#progressBar and div#bar then prepend to $("#owl-demo")
    function buildProgressBar() {
        $progressBar = $("<div>", {
            id: "progressBar"
        });
        $bar = $("<div>", {
            id: "bar"
        });
        $progressBar.append($bar).prependTo("#mobileAppBody");
    }
    function start() {
        //reset timer
        percentTime = 0;
        isPause = false;
        //run interval every 0.01 second
        tick = setInterval(interval, 10);
    }

    function interval() {
        var activeIndex = $('#mobileSlider').find('.active').index();
        var activeLi = $('#appNav li').get(activeIndex);
        //console.log(activeIndex);

        if (isPause === false) {
            percentTime += 1 / time;
            $(activeLi).css({
                opacity: percentTime/100
            });
            //$(activeLi).fadeIn(700);
            $bar.css({
                width: percentTime + "%"
            });
            //if percentTime is equal or greater than 100
            if (percentTime >= 100) {
                //slide to next item
                //owlData.trigger('owl.next')
                owlApp.trigger('owl.next');

            }
        }
    }
    //moved callback
    function moved() {
        //clear interval
        clearTimeout(tick);
        //start again
        start();
    }
    //Adding accordeon for scheme block
    $("#accordion").accordion({
        //collapsible: true
        //icons: false
        //TODO: Enable this event before prodaction
        heightStyle: "content",
        event: "click hoverintent"
    });
    //Change text to price in ico block
    $('.wrapLists li').hover(
        function () {
            $(this).find('.iconText').hide();
            $(this).find('.iconPrice').fadeIn('500');
        }, function () {
            $(this).find('.iconText').fadeIn('500');
            $(this).find('.iconPrice').hide();
        }
    );
    //Start video
    var player = new MediaElementPlayer('#videoPlayer');
    $('.playButton img').click(function () {
        //var currentHeight = $(this).closest('.wrap').height();
        //var height30 = '130%;
        var currentWindowHeight = $(window).height();
        //$('#videoBlock').css('padd');
        $('#pult').fadeOut(800);
        $('#videoText').fadeOut(800);
        $('.playButton').fadeOut(800);
        $('#player').height(currentWindowHeight);
        $('#player').fadeIn(800);
        player.play();
    });
    //Stop video
    $('#videoPlayer').click(function () {
        player.pause();
        $('#player').fadeOut(200);
        $('#pult').fadeIn(200);
        $('#videoText').fadeIn(200);
        $('.playButton').fadeIn(200);
    });
    //Add mask for phone field
    $('input.phone').mask("8 (999) 999-9999");
    //Add scrollUp
    $.scrollUp({scrollText:''});
    //Add main form validation&ajax sending
    $('#mainForm').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        locale: 'ru_Ru',
        err: {
            // You can set it to popover
            // The message then will be shown in Bootstrap popover
            container: 'tooltip'
        },
        fields: {
            username: {
                validators: {
                    notEmpty: {
                        message: 'Вы должны заполнить это поле!'
                    },
                    stringLength: {
                        min: 4,
                        max: 30,
                        message: 'Ваше имя должно быть от 4 до 30 символов'
                    },
                    regexp: {
                        regexp: /^[а-яА-Я]+$/,
                        message: 'Введите имя на русской раскладке'
                    }
                }
            },
            phone: {
                validators: {
                    notEmpty: {
                        message: 'Вы должны заполнить это поле!'
                    }
                }
            }
        }
    })
        .on('success.form.fv', function (e) {
            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the FormValidation instance
            var bv = $form.data('formValidation');

            $(this).find('.btn').html('Спасибо за вашу заявку!');
            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function (result) {
                // ... Process the result ...
            }, 'json');
            $form.find('input').prop( "disabled", true );
            console.log($form);
        });
    //Add askForm form validation&ajax sending
    $('#askForm').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        locale: 'ru_Ru',
        err: {
            // You can set it to popover
            // The message then will be shown in Bootstrap popover
            container: 'tooltip'
        },
        fields: {
            username: {
                validators: {
                    notEmpty: {
                        message: 'Вы должны заполнить это поле!'
                    },
                    stringLength: {
                        min: 4,
                        max: 30,
                        message: 'Ваше имя должно быть от 4 до 30 символов'
                    },
                    regexp: {
                        regexp: /^[а-яА-Я]+$/,
                        message: 'Введите имя на русской раскладке'
                    }
                }
            },
            phone: {
                validators: {
                    notEmpty: {
                        message: 'Вы должны заполнить это поле!'
                    }
                }
            },
            message: {
                validators: {
                    notEmpty: {
                        message: 'Вы должны заполнить это поле!'
                    },
                    stringLength: {
                        min: 0,
                        max: 500,
                        message: 'Ваше сообщение слишком длинное!'
                    }
                }
            }

        }
    })
        .on('success.form.fv', function (e) {
            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the FormValidation instance
            var bv = $form.data('formValidation');

            $(this).find('.btn').html('Спасибо за вашу заявку!');
            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function (result) {
                // ... Process the result ...
            }, 'json');
            $form.find('input').prop( "disabled", true );
            $form.find('textarea').prop( "disabled", true );
            console.log($form);
        });
    //Complect page calc
    function calcInit(){
        var thisItem = $('fieldset[data-cat="base"]');
        $(thisItem).each(function(){
            var thisNum = $(this).find('.num').val();
            //$(this).find('.num').val(1);
            $(this).find('input[type="checkbox"]').trigger('click');
            addToAllSumm(this, thisNum, 'base');
        });
    }
    calcInit();
    //Plus and minus calculation
    $('span.plus').click(function () {
        checker(this, 'plus');
    });
    $('span.minus').click(function () {
        checker(this, 'minus');
    });
    //Add one if cheker is cheked
    $('#complectCalc input[type="checkbox"]').click(function () {
        checker(this, 'checkbox');
    });
    //main calc finction
    function checker($this, $type){
        //console.log($this);
        var thisInput = $($this).parent().find('.num'); //Current input with num
        var thisChckbx = $($this).parent().find('input[type="checkbox"]').is(':checked') ; //Current checkbox active
        var typeOfField = $($this).parent().attr('data-cat'); //type - complect or addition
        var currentVal = parseInt(thisInput.val()); //current value in int
        if ( thisChckbx ){
            //if( $type == 'checkbox') {
            //    if (currentVal == 0){
            //        thisInput.val(1); //set to 1
            //    }
           if ( $type == 'plus' ){
                thisInput.val(currentVal + 1); //add one more
                addToAllSumm($this, 1, typeOfField);
            }else if( $type == 'minus' ){
                if (currentVal != 0){
                    thisInput.val(currentVal - 1); //remove one
                    addToAllSumm($this, -1, typeOfField);
                }
            }
        }else{
            if ( $type == 'plus' ){
                thisInput.val(currentVal + 1); //add one more
            }else if( $type == 'minus' ){
                if (currentVal != 0){
                    thisInput.val(currentVal - 1); //remove one
                }
            }
        }
        if ( ($type == 'checkbox') && (thisChckbx) ){
            if (currentVal == 0){
                thisInput.val(1); //set to 1
                addToAllSumm($this, 1, typeOfField);
            }else{
                addToAllSumm($this, currentVal, typeOfField);
            }
        }else if( ($type == 'checkbox') && (!thisChckbx) ){
            if (currentVal == 1){
                addToAllSumm($this, -1, typeOfField);
                thisInput.val(0); //set to 0
            }else{
                addToAllSumm($this, -1*currentVal, typeOfField);
            }
        }
    }
    function addToAllSumm($this, $currentVal, $typeOfField){
        if ( $typeOfField == 'base' ){

            var baseSum = $('.baseSum .sum').find('.sumNum');
            var baseAbon = $('.baseSum .abon').find('.sumNum');
            var basePrice = $($this).parent().find('.complPrice').val();
            var baseAbonVal = $($this).parent().find('.complAbon').val();

            var baseSumCalc = parseInt(baseSum.html()) + parseInt(basePrice)*$currentVal;
            var baseAbonCalc = parseInt(baseAbon.html()) + parseInt(baseAbonVal)*$currentVal;

            baseSum.html(baseSumCalc);
            baseAbon.html(baseAbonCalc);
        }else if( $typeOfField == 'add' ){
            var addSum = $('.addSum .sum').find('.sumNum');
            var addAbon = $('.addSum .abon').find('.sumNum');
            var addPrice = $($this).parent().find('.addPrice').val();
            var addAbonVal = $($this).parent().find('.addAbon').val();

            var addSumCalc = parseInt(addSum.html()) + parseInt(addPrice)*$currentVal;
            var addAbonCalc = parseInt(addAbon.html()) + parseInt(addAbonVal)*$currentVal;

            addSum.html(addSumCalc);
            addAbon.html(addAbonCalc);
        }
    }
    //sheme img popover
    $(".innerPage [rel='popover']").popover({
        trigger: "manual",
        html: true,
        //content: $(this).children('.popoverContent').html()
        //content: getPopoverContent(this.children('.popoverContent').html())
        content: function () {
            var content = $(this).children('.popoverContent').html()
            //var clone = $($(this).data('popover-content')).clone(true).removeClass('hide');
            return content;
        }
    })
        .on("mouseenter", function () {

            console.log($(this).children('.popoverContent').html());
            var _this = this;
            $(this).popover("show");
            $(".popover").on("mouseleave", function () {
                $(_this).popover('hide');
            });
        }).on("mouseleave", function () {
            var _this = this;
            setTimeout(function () {
                if (!$(".popover:hover").length) {
                    $(_this).popover("hide")
                }
            }, 500);
        });
    //sheme img on main page
    $( ".mainPage [rel='popover']" ).hover(function() {
        console.log($(this).attr("data-mark"));
        var dataMark = $(this).attr("data-mark");
        $('#accordion h3[data-mark="'+ dataMark +'"]').trigger( "click" );
        //$.scrollTo('#accordion', 1000);
    });
    //$( ".mainPage [rel='popover']" ).click(function() {
    //    $.scrollTo('#accordion h3[data-mark="'+ ($(this).index()+1) +'"]', 1000);
    //});
    $('#accordion h3').hover(function() {
        var dataMark = $(this).attr("data-mark");
        $('.imgMarks li[data-mark="'+ dataMark +'"]').toggleClass("hovered");
        //$.scrollTo(thisId);
        //$.scrollTo('#schemeImg', 1000);
    });
    $('#accordion div').hover(function() {
        var dataMark = $(this).prev('h3').attr("data-mark");
        $('.imgMarks li[data-mark="'+ dataMark +'"]').toggleClass("hovered");
    });
    defineWindowWidth();
    defineScmImg();

    $("#complectSlider").owlCarousel({
        autoPlay: 3000, //Set AutoPlay to 3 seconds
        items : 4
    });
});